const User = require('../models/User');
const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator');
const userService = require('../services/userService');
const jwtService = require('../services/jwtService');
const ApiError = require('../utils/apiError');

class authController {
    async registration(req, res, next) {
        try {
            const error = validationResult(req);
            if (!error.isEmpty()) {
                next(ApiError.RegistrationError(error.errors));
            }
            const {username, password} = req.body;
            const newUser = await userService.createUser(username, password);

            const token = jwtService.login(newUser._id, newUser.username);
            return res.status(200).json({message: "Пользователь успешно зарегистрирован", token});
        } catch (e) {
            console.error(e);
            next(ApiError.RegistrationError());
        }
    }

    async login(req, res, next) {
        try {
            const {username, password} = req.body;
            const user = await userService.getUser(username);
            if (!user) {
                next(ApiError.BadRequest(`Пользователь ${username} не найден`));
            }
            const validPassword = bcrypt.compareSync(password, user.password);
            if (!validPassword) {
                next(ApiError.BadRequest('Введен неверный пароль'));
            }
            const token = jwtService.login(user._id, user.username);
            return  res.status(200).json({token});
        } catch (e) {
            console.error(e);
            next(ApiError.BadRequest('Введен неверный пароль'));
        }
    }

    async getUsers(req, res, next) {
        try {
            const users = await userService.getUsers();
            return res.status(200).json(users);
        } catch (e) {
            next(e);
        }
    }

    async getInfo (req, res, next) {
        try {
            return res.json(req.user.username);
        } catch (e) {
            next(e);
        }
    }
}

module.exports = new authController();
