const express = require('express');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser')
const apiRouter = require('./routers/apiRouter');
const errorMiddleware = require('./middlewares/errorMiddleware');
const PORT = 5050;
const cors = require('cors');

const app = express();
app.use(cors({origin: '*'}));
app.use(express.json());
app.use(cookieParser());
app.use("/api", apiRouter);
app.use(errorMiddleware);

const start = async () => {
    try {
        await mongoose.connect(`mongodb+srv://sa:1qaZXsw2@cluster0.gilda.mongodb.net/test?retryWrites=true&w=majority`);
        app.listen(PORT, () => console.log(`server started on port ${PORT}`));
    } catch (e) {
        console.log(e);
    }
}

start();

