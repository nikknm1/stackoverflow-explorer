const jwtService = require('../services/jwtService');
const ApiError = require('../utils/apiError');

module.exports = function (req, res, next) {
    try {
        if (!req.headers.authorization) throw ApiError.UnauthorizedError();
        const token = req.headers.authorization.split(' ')[1];
        if (!token) throw ApiError.UnauthorizedError();

        req.user = jwtService.verify(token);
        req.token = token;
        next();
    } catch (e) {
        console.error(e);
        throw ApiError.UnauthorizedError();
    }
};
