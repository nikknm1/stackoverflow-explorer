const Router = require('express');
const router = new Router();
const controller = require('../controllers/authController');
const {check} = require("express-validator");
const authMiddleware = require('../middlewares/authMiddleware');

router.post('/registration', [
    check('username', "Имя пользователя не может быть пустым").notEmpty(),
    check('password', "Пароль должен быть больше 2 и меньше 10 символов").isLength({min:2, max:10})
], controller.registration);
router.post('/login', controller.login);
router.get('/info', authMiddleware, controller.getInfo);

module.exports = router;
