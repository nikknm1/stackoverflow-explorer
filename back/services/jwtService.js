const jwt = require('jsonwebtoken');
const {secret} = require("../config");

class JwtService {
    login(id, username) {
        const payload = {
            id,
            username
        };
        return jwt.sign(payload, secret, {expiresIn: "6h"});
    }

    verify(token) {
        return jwt.verify(token, secret);
    }
}

module.exports = new JwtService();
