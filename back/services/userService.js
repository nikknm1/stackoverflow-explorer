const User = require('../models/User');
const bcrypt = require('bcryptjs');
const ApiError = require('../utils/apiError');
const jwtService = require('./jwtService');


class UserService {
    async createUser(username, password) {
        try {
            const candidate = await User.findOne({username})
            if (candidate) {
                throw ApiError.BadRequest(`Пользователь с почтовым адресом ${username} уже существует`)
            }
            const hashPassword = bcrypt.hashSync(password, 7);
            const user = new User({username, password: hashPassword});
            const newUser = await user.save();
            return jwtService.login(newUser._id, newUser.username);
        } catch (e) {
            console.error(e);
            throw ApiError.BadRequest(`Ошибка при регистрации`)
        }
    }

    async getUser(username) {
        try {
            return await User.findOne({username});
        } catch (e) {
            console.error(e);
            return e;
        }
    }

    async getUsers() {
        try {
            return await User.find();
        } catch (e) {
            console.error(e);
            return e;
        }
    }
}

module.exports = new UserService();