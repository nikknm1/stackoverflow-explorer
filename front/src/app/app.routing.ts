import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {Routes, RouterModule} from '@angular/router';

import {AuthLayoutComponent} from './layouts/auth-layout/auth-layout.component';
import {UserLayoutComponent} from './layouts/user-layout/user-layout.component';
import { AuthGuard } from './helpers';

const routes: Routes = [
  {
    path: '',
    component: AuthLayoutComponent,
    loadChildren: './layouts/auth-layout/auth-layout.module#AuthLayoutModule'
  },
  {
    path: '',
    component: UserLayoutComponent,
    canActivate: [AuthGuard],
    loadChildren: './layouts/user-layout/user-layout.module#UserLayoutModule'
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
