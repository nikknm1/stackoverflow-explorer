import {Component, Input, OnInit} from '@angular/core';
import {StackoverflowService} from 'src/app/services/stackoverflow.service';
import {ModalService} from 'src/app/services/modal.service';
import {Owner} from 'src/app/models/question';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.scss']
})
export class AuthorComponent implements OnInit {
  @Input() author: Owner;

  constructor(
    private stackoverflowService: StackoverflowService,
    private modalService: ModalService
  ) {
  }

  ngOnInit(): void {
  }

  clickAuthor(): void {
    this.modalService.open('searchModal', {
      type: 'author',
      value: this.author.user_id,
      title: this.author.display_name
    });
  }
}
