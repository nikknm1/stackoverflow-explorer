import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services';


@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  @Input() userName: string = "";
  @Input() searchValue: string;
  @Output() search = new EventEmitter<string>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.searchValue = params['q'];
      this.clickSearch();
    });
  }

  clickSearch(): void {
    if (this.searchValue) {
      this.goToSearch(this.searchValue);
    }
  }

  goToSearch(searchValue: string): void {
    this.router.navigate(['/results'], {queryParams: {q: searchValue}});
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
