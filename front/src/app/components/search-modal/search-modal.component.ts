import {Component, ElementRef, Input, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {StackoverflowService} from 'src/app/services/stackoverflow.service';
import {Question, SearchData} from 'src/app/models/question';
import {ModalService} from '../../services/modal.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-search-modal',
  templateUrl: './search-modal.component.html',
  styleUrls: ['./search-modal.component.scss']
})
export class SearchModalComponent implements OnInit, OnDestroy {
  private element: any;
  @Input() id: string;
  @ViewChild('searchModal') searchModalRef: ElementRef;
  items: SearchData<Question>;
  loading: boolean;
  isOpened: boolean;
  searchBy: string;
  searchValue: string;

  constructor(
    private modalService: ModalService,
    private el: ElementRef,
    private stackoverflowService: StackoverflowService,
    private modalOpener: NgbModal
  ) {
    this.element = el.nativeElement;
    this.isOpened = false;
  }

  ngOnInit(): void {
    this.initPanel();
  }

  initPanel(): void {
    this.modalService.add(this);
  }

  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  open(params: any): void {
    if (!params) {
      this.loading = false;
      this.items = null;
      this.searchBy = '';
      this.searchValue = '';
      return;
    }
    this._loadItems(params.type, params.value, params.title);
  }

  _loadItems(type, value, title) {
    this.loading = true;
    this.modalOpener.open(this.searchModalRef);
    this.isOpened = true;
    if (type === 'tag') {
      this.searchBy = 'тэгу';
      this.searchValue = title;
      this.stackoverflowService.getMostPopularQuestionsByTag(value).subscribe(items => {
        this.items = items;
        this.loading = false;
      });
    } else if (type === 'author') {
      this.searchBy = 'автору';
      this.searchValue = title;
      this.stackoverflowService.getMostPopularQuestionsByAuthor(value).subscribe(items => {
        this.items = items;
        this.loading = false;
      });
    }
  }

  close(): void {
    this.modalOpener.dismissAll();
  }
}
