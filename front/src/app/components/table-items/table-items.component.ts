import {Component, Input} from '@angular/core';
import {Question, Owner, SearchData} from 'src/app/models/question';

@Component({
  selector: 'app-table-items',
  templateUrl: './table-items.component.html',
  styleUrls: ['./table-items.component.scss']
})
export class TableItemsComponent {
  @Input() items: SearchData<Question>;
  @Input() loading: boolean;
  @Input() query: string;
  sortASC: boolean;
  selectedSort: string;

  constructor() {
    this.sortASC = true;
  }

  sortBy(prop: string): void {
    if (!this.items) {
      return;
    }
    if (this.selectedSort === prop) {
      this.sortASC = !this.sortASC;
    }
    this.selectedSort = prop;
    if (prop === 'author') {
      this.items?.items.sort((item1, item2) => {
        const owner1: Owner = item1.owner;
        const owner2 = item2.owner;
        if (!this.sortASC) {
          return owner2.display_name.localeCompare(owner1.display_name);
        }
        return owner1.display_name.localeCompare(owner2.display_name);
      });
      return;
    }
    if (prop === 'title') {
      this.items?.items.sort((item1, item2) => {
        if (!this.sortASC) {
          return item2.title.localeCompare(item1.title);
        }
        return item1.title.localeCompare(item2.title);
      });
    }
    if (prop === 'answer_count') {
      this.items?.items.sort((item, item2) => {
        if (!this.sortASC) {
          return item2.answer_count - item.answer_count;
        }
        return item.answer_count - item2.answer_count;
      });
    }
  }
}
