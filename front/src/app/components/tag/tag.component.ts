import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ModalService} from 'src/app/services/modal.service';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss']
})
export class TagComponent {
  @Input() tag: string;
  @Output() tagClicked = new EventEmitter<string>();

  constructor(
    private modalService: ModalService
  ) {
  }

  clickTag() {
    this.tagClicked.emit(this.tag);
    this.modalService.open('searchModal', {
      type: 'tag',
      title: this.tag,
      value: this.tag
    });
  }
}
