import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'answerCount'})
export class AnswerCountPipe implements PipeTransform {
  transform(value: number): string {
    if (!value && value !== 0) {
      return '';
    }
    return value + ' ' + this._getTextAnswer(value);
  }

  _getTextAnswer(count: number): string {
    if (count) {
      const n = Math.abs(count) % 100;
      const n1 = n % 10;
      if (n > 10 && n < 20) {
        return 'Ответов';
      }
      if (n1 > 1 && n1 < 5) {
        return 'Ответа';
      }
      if (n1 === 1) {
        return 'Ответ';
      }
      return 'Ответов';
    }
    return '';
  }
}
