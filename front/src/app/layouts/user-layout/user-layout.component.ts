import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {AuthService} from '../../services';

@Component({
  selector: 'app-results-layout',
  templateUrl: './user-layout.component.html',
  styleUrls: ['./user-layout.component.scss']
})
export class UserLayoutComponent implements OnInit {
  userName: string;

  constructor(
    private location: Location,
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.authService.getInfo().subscribe((username) => {
      this.userName = username;
    });
  }

  get isHomePage(): boolean {
    const currentPage = this.location.path();
    return currentPage === '/home';
  }

  goBack(): void {
    this.location.back();
  }
}
