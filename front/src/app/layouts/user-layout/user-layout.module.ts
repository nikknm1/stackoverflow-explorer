import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ComponentsModule} from '../../components/components.module';
import {DetailsComponent} from '../../pages/details/details.component';
import {SearchHomeComponent} from '../../pages/search-home/search-home.component';
import {SearchResultsComponent} from '../../pages/search-results/search-results.component';

import {UserLayoutRouting} from './user-layout.routing';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [DetailsComponent, SearchHomeComponent, SearchResultsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(UserLayoutRouting),
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule
  ]
})

export class UserLayoutModule {
}
