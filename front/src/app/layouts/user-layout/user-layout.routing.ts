import {Routes} from '@angular/router';
import {DetailsComponent} from '../../pages/details/details.component';
import {SearchHomeComponent} from '../../pages/search-home/search-home.component';
import {SearchResultsComponent} from '../../pages/search-results/search-results.component';

export const UserLayoutRouting: Routes = [
  {path: 'details', component: DetailsComponent},
  {path: 'details/:id', component: DetailsComponent},
  {path: 'home', component: SearchHomeComponent},
  {path: 'results', component: SearchResultsComponent},
  { path: '**', redirectTo: '/home', pathMatch: 'full' }
];
