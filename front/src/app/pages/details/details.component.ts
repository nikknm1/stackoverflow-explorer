import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {StackoverflowService} from 'src/app/services/stackoverflow.service';
import {Answer, Question, SearchData} from 'src/app/models/question';
import {fadeAnimation} from '../animations/animations';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  animations: [
    fadeAnimation
  ]
})
export class DetailsComponent implements OnInit {
  id: number;
  answers: SearchData<Answer>;
  question: Question;
  loading = false;

  constructor(
    private stackoverflowService: StackoverflowService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = +params.get('id');
      this.getQuestionDetails(this.id);
    });
  }

  getQuestionDetails(questionId: number) {
    this.loading = true;
    this.stackoverflowService.getQuestion(questionId).subscribe(data => {
      this.question = data.items[0];
    });
    this.stackoverflowService.questionAnswers(questionId).subscribe(data => {
      this.answers = data;
      this.loading = false;
    });
  }
}
