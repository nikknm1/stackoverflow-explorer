import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';

import {AuthService} from '../../services';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  registrationForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  errorMessage: string;
  error = null;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthService
  ) {
    if (localStorage.getItem('token')) {
      this.router.navigate(['/home']);
    }
  }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
  }

  get usernameError() {
    let usernameError = '';
    const usernameForm = this.registrationForm.controls.username;
    if (this.submitted && usernameForm.errors && usernameForm.errors.required) {
      usernameError = 'Обязательное поле';
    }
    if (this.error && this.error.errors) {
      const usernameErrorResult = this.error.errors.find(er => er.param === 'username');
      if (usernameErrorResult) {
        usernameError = usernameErrorResult.msg;
      }
    }

    return usernameError;
  }

  get passwordError() {
    let passwordError = '';
    const passwordForm = this.registrationForm.controls.password;
    if (this.submitted && passwordForm.errors && passwordForm.errors.required) {
      passwordError = 'Обязательное поле';
    }
    if (this.error && this.error.errors) {
      const passwordErrorResult = this.error.errors.find(er => er.param === 'password');
      if (passwordErrorResult) {
        passwordError = passwordErrorResult.msg;
      }
    }

    return passwordError;
  }

  registration() {
    this.submitted = true;
    if (this.registrationForm.invalid) {
      return;
    }

    this.loading = true;
    const username = this.registrationForm.controls.username.value;
    const password = this.registrationForm.controls.password.value;
    this.authenticationService.registration(username, password)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
          this.errorMessage = error.message;
          this.loading = false;
        });
  }
}
