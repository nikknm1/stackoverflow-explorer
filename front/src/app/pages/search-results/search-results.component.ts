import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StackoverflowService} from 'src/app/services/stackoverflow.service';
import {Question, Owner, SearchData} from 'src/app/models/question';
import {fadeAnimation} from '../animations/animations';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss'],
  animations: [
    fadeAnimation
  ]
})
export class SearchResultsComponent implements OnInit {
  items: SearchData<Question>;
  sortASC: boolean;
  selectedSort: string;
  query: string;
  loading = false;

  constructor(
    private stackoverflowService: StackoverflowService,
    private route: ActivatedRoute,
  ) {
    this.sortASC = true;
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.query = params['q'];
      this.search(this.query);
    });
  }

  search(searchValue: string) {
    this.loading = true;
    this.stackoverflowService.search(searchValue).subscribe(data => {
      this.items = data;
      this.loading = false;
    });
  }
}
