import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthService {

  constructor(
    private http: HttpClient
  ) {}

  login(username: string, password: string) {
    return this.http.post<any>(`${environment.loginUrl}/api/login`, { username, password }).pipe(map(result => {
      localStorage.setItem('token', result.token);
      return result;
    }));
  }

  registration(username: string, password: string) {
    return this.http.post<any>(`${environment.loginUrl}/api/registration`, { username, password }).pipe(map(result => {
      localStorage.setItem('token', result.token);
      return result;
    }));
  }

  getInfo() {
    return this.http.get<any>(`${environment.loginUrl}/api/info`, {});
  }

  logout() {
    localStorage.removeItem('token');
  }
}
