import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Answer, Question, SearchData} from 'src/app/models/question';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StackoverflowService {
  constructor(
    private http: HttpClient
  ) {}

  search(searchValue: string) {
    const api = `${environment.stackoverflowApiUrl}search?order=desc&sort=relevance&intitle=${encodeURIComponent(searchValue)}&site=stackoverflow`;
    return this.http.get<SearchData<Question>>(api);
  }

  questionAnswers(questionId: number) {
    const api = `${environment.stackoverflowApiUrl}questions/${questionId}/answers?order=desc&sort=votes&site=stackoverflow&filter=withbody`;
    return this.http.get<SearchData<Answer>>(api);
  }

  getQuestion(questionId: number){
    const api = `${environment.stackoverflowApiUrl}questions/${questionId}?order=desc&sort=activity&site=stackoverflow&filter=withbody`;
    return this.http.get<SearchData<Question>>(api);
  }

  getMostPopularQuestionsByTag(tag: string) {
    const api = `${environment.stackoverflowApiUrl}search?order=desc&sort=votes&tagged=${encodeURIComponent(tag)}&site=stackoverflow`;
    return this.http.get<SearchData<Question>>(api);
  }

  getMostPopularQuestionsByAuthor(authorId: number) {
    const api = `${environment.stackoverflowApiUrl}users/${authorId}/questions?order=desc&sort=votes&site=stackoverflow`;
    return this.http.get<SearchData<Question>>(api);
  }
}
