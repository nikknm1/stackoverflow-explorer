export const environment = {
  production: true,
  stackoverflowApiUrl: 'https://api.stackexchange.com/2.2/',
  loginUrl: 'http://127.0.0.1:5050'
};
